package category


import (
	"github.com/kanwulf01/go-hexagonal-arq-test/helpers"
	"database/sql"
	"encoding/json"
	// "github.com/go-chi/chi"
	// "log"
	"fmt"
	"net/http"
	// "strconv"
	"github.com/kanwulf01/go-hexagonal-arq-test/category/gateway"
	models "github.com/kanwulf01/go-hexagonal-arq-test/category/models"
	"strings"
	//"github.com/gorilla/mux"
	//"github.com/go-chi/chi"
	//"strconv"
)


type CategoryHTTPService struct {
	gwt category.CategoryGateway
}

type DataCategoria struct {
	Success bool          `json:"success"`
	DataCategorias    []models.Categoria `json:"categorias"`
	Errors  []string      `json:"errors"` 
}

type Categoria struct {
	Success bool          `json:"success"`
	Categoria    models.Categoria `json:"categoria"`
	Errors  []string      `json:"errors"` 
}

//crear funcion que injecta la base de datos al modulo gateway

func NewCategoryHTTPService(db *sql.DB) *CategoryHTTPService {
	return &CategoryHTTPService{
		category.NewCategoryGateway(db),
	}
}

//Controller para crear Respuestas(answers)
func (s *CategoryHTTPService) CreateCategoryHandler(w http.ResponseWriter, r *http.Request) {
	bodyCategory, success := helpers.DecodeBodyCategory(r)
	fmt.Println(bodyCategory)
	if success != true {
		http.Error(w, "could not decode body", http.StatusBadRequest)
		return
	}

	var Categoria = Categoria{Errors: make([]string, 0)}
	bodyCategory.Nombre = strings.TrimSpace(bodyCategory.Nombre)
	bodyCategory.Tipo = strings.TrimSpace(bodyCategory.Tipo)
	bodyCategory.Categoria_padre = strings.TrimSpace(bodyCategory.Categoria_padre)

	if !helpers.IsValidField(bodyCategory.Nombre) && !helpers.IsValidField(bodyCategory.Tipo) && !helpers.IsValidFieldNumeric(bodyCategory.Puntosnecesarios) && !helpers.IsValidField(bodyCategory.Categoria_padre) {
		Categoria.Success = false
		Categoria.Errors = append(Categoria.Errors, "invalid fields")

		json, _ := json.Marshal(Categoria)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	}
	//llama la funcion del storage que usa sql nativo

	res,err := s.gwt.CreateCategory(&bodyCategory)
	if err != true {
		Categoria.Errors = append(Categoria.Errors, "Cannot Create a Category")
	}
	//fmt.Println(res)
	//si todo sale bien y la query funciona encapsulo dentro del objeto

	Categoria.Success = true
	Categoria.Categoria = *res

	json,_ := json.Marshal(Categoria)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
	return


}