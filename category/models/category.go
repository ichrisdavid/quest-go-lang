package category

import (
	"time"
)

//Objeto Categoria
type Categoria struct {
	ID 	int64 	`json:"id"`
	Categoria_uid string `json:"uid"`
	Nombre 	string `json:"nombre"`
	Tipo string `json:"tipo"`
	Puntosnecesarios int64 `json:"puntosnecesarios"`
	Time time.Time `json:"creation_time"`
	Id_usuario_admin string `json:"id_usuario_admin"`
	Categoria_padre string `json:"categoria_padre"`

}
