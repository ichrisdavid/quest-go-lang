package category


import (
	"fmt"
	"database/sql"
	//"github.com/kanwulf01/go-hexagonal-arq-test/questions/models"
	//"github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
	"github.com/kanwulf01/go-hexagonal-arq-test/category/models"
	"log"
	//"time"	
	//"github.com/google/uuid"
	//"github.com/satori/go.uuid"
)

type CategoryStorage interface {
	createCategoryDB(c *category.Categoria) (*category.Categoria,bool)
	//getCategoryBD()() ([]*category.Categoria)
	//getCategoryByIDBD(id int64)(c *category.Categoria) (*category.Categoria)
}


type CategoryService struct {
	db *sql.DB
}

func NewCategoryStorageGateway(db *sql.DB) CategoryStorage {
	return &CategoryService{db:db}
}




func (s *CategoryService) createCategoryDB(c *category.Categoria) (*category.Categoria,bool) { 
	var categoria_id int64 
	
	 
	
	//var timestamp time.Time
	s.db.QueryRow("INSERT INTO Categoria(categoria_uid,nombre,tipo,puntosnecesarios,id_usuario_admin,categoria_padre) VALUES(uuid_generate_v4(),$1,$2,$3,$4,$5) RETURNING id",c.Nombre, c.Tipo,c.Puntosnecesarios,c.Id_usuario_admin, c.Categoria_padre).Scan(&categoria_id)
	if categoria_id != 0 {
		log.Printf("cannot save the category")
		return nil, false
	}

	fmt.Println(categoria_id)

	return &category.Categoria{//retorno una puntero direccion de tipo answer
		ID: 				categoria_id,
		Nombre:				c.Nombre,
		Tipo:				c.Tipo,
		Puntosnecesarios:	c.Puntosnecesarios,
		Id_usuario_admin: 	c.Id_usuario_admin,
		Categoria_padre:	c.Categoria_padre,
	}, true
 }