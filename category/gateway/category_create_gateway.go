package category

import (
	"database/sql"
	"github.com/kanwulf01/go-hexagonal-arq-test/category/models"
)


type CategoryGateway interface {
	CreateCategory(c *category.Categoria) (*category.Categoria, bool)
}

type CreateCategoryInBD struct {
	CategoryStorage
}


func NewCategoryGateway(db *sql.DB) CategoryGateway {
	return &CreateCategoryInBD{NewCategoryStorageGateway(db)}
}

func (c *CreateCategoryInBD) CreateCategory(categoria *category.Categoria) (*category.Categoria, bool) {
	return c.createCategoryDB(categoria)
}