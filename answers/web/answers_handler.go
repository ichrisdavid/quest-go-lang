package answers
//CONTROLLERS

import (
	"github.com/kanwulf01/go-hexagonal-arq-test/helpers"
	"database/sql"
	"encoding/json"
	// "github.com/go-chi/chi"
	// "log"
	"fmt"
	"net/http"
	// "strconv"
	"github.com/kanwulf01/go-hexagonal-arq-test/answers/gateway"
	models "github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
	"strings"
	//"github.com/gorilla/mux"
	//"github.com/go-chi/chi"
	//"strconv"
)

//Objeto que llame a la interface del modulo gateway
type AnswerHTTPService struct {
	gwt answers.AnswerGateway
}

type DataRespuesta struct {
	Success bool          `json:"success"`
	DataRespuesta    []models.Answer `json:"respuesta"`
	Errors  []string      `json:"errors"` 
}

type Respuesta struct {
	Success bool          `json:"success"`
	Respuesta    models.Answer `json:"respuesta"`
	Errors  []string      `json:"errors"` 
}


//crear funcion que injecta la base de datos al modulo gateway

func NewAnswerHTTPService(db *sql.DB) *AnswerHTTPService {
	return &AnswerHTTPService{
		answers.NewAnswerGateway(db),
	}
}
//Controller para crear Respuestas(answers)
func (s *AnswerHTTPService) CreateAnswerHandler(w http.ResponseWriter, r *http.Request) {
	bodyAnswer, success := helpers.DecodeBodyRespuesta(r)
	if success != false {
		http.Error(w, "could not decode body", http.StatusBadRequest)
		return
	}

	var Respuesta = Respuesta{Errors: make([]string, 0)}
	bodyAnswer.Respuesta = strings.TrimSpace(bodyAnswer.Respuesta)
	fmt.Println(bodyAnswer)

	if !helpers.IsValidField(bodyAnswer.Respuesta) && !helpers.IsValidField(bodyAnswer.Id_pregunta) {
		Respuesta.Success = false
		Respuesta.Errors = append(Respuesta.Errors, "invalid description")

		json, _ := json.Marshal(Respuesta)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	}
	//llama la funcion del storage que usa sql nativo

	res,err := s.gwt.CreateAnswer(&bodyAnswer)
	if err != true {
		Respuesta.Errors = append(Respuesta.Errors, "Cannot Create a Answer")
	}

	//si todo dale bien y la query funciona encapsulo dentro del objeto

	Respuesta.Success = true
	Respuesta.Respuesta = *res

	json,_ := json.Marshal(Respuesta)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
	return


}



