package answers

import (
	"database/sql"
	"github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
)

//Interface para injectar los casos de uso del modelo Answer desde storage por gateway
type AnswerGateway interface {
	CreateAnswer(a *answers.Answer) (*answers.Answer, bool)
}

//Objeto que llama la interface de storage donde estan los casos de uso propios del modelo answer
type CreateAnswerInDB struct {
	AnswerStorage
}

func NewAnswerGateway(db *sql.DB) AnswerGateway {
	return &CreateAnswerInDB{NewAnswerStorageGateway(db)}
}

func (c *CreateAnswerInDB) CreateAnswer(a *answers.Answer) (*answers.Answer, bool) {
	return c.createAnswerBD(a)
}
