package answers

import (
	//"fmt"
	"database/sql"
	"github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
	"log"
)
//Permite usar estos casos de uso fuera de este modulo 
type AnswerStorage interface {
	createAnswerBD(a *answers.Answer) (*answers.Answer, bool)
	//de mas metodos que manipulen el modelo answers
}


//permite la injeccion de la base de datos al modulo
type AnswerService struct {
	db *sql.DB
}

//Permite el paso de la base de datos por el service
func NewAnswerStorageGateway(db *sql.DB) AnswerStorage {
	return &AnswerService{db: db}
}

//Metodo del tipo Answer Service para crear una answer
func (s *AnswerService) createAnswerBD(a *answers.Answer) (*answers.Answer, bool) {
	var answer_id int64 // Instancio la variable donde guardare el id del objeto creado en bd
	s.db.QueryRow("INSER INTO respuesta(respuesta,id_pregunta) VALUES($1,$2) RETURNING id",a.Respuesta,a.Id_pregunta).Scan(&answer_id)
	if answer_id != 0 {
		log.Printf("cannot save the answer, %s")
		return nil, false
	}

	return &answers.Answer{//retorno una puntero direccion de tipo answer
		ID: 		answer_id,
		Respuesta:	a.Respuesta,
		Id_pregunta:		a.Id_pregunta,
	}, true

}


