package api

import (
	"github.com/kanwulf01/go-hexagonal-arq-test/internal/storage"
	questions "github.com/kanwulf01/go-hexagonal-arq-test/questions/web"
	answers "github.com/kanwulf01/go-hexagonal-arq-test/answers/web"
	category "github.com/kanwulf01/go-hexagonal-arq-test/category/web"
)


func Start(port string) {
	db := storage.GetConnection()
	defer db.Close()

	r := routes(questions.NewQuestionHTTPService(db), answers.NewAnswerHTTPService(db), category.NewCategoryHTTPService(db))

	
	//instancia la clase server como parametro y untipo de rutas
	//ingresa puerto como para
	server := newServer(port, r)

	server.Start()
}
