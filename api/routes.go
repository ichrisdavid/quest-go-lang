package api


import (
	"github.com/go-chi/chi"
	"github.com/kanwulf01/go-hexagonal-arq-test/questions/web"
	"github.com/kanwulf01/go-hexagonal-arq-test/answers/web"
	"github.com/kanwulf01/go-hexagonal-arq-test/category/web"
)

//la funcion router deberia recibir como parametro cada service dependiendo del modelo que se trate para
//poder implementar los endpoints de cada modelo
func routes(services *questions.QuestionHTTPService, service *answers.AnswerHTTPService, servicescategory *category.CategoryHTTPService) *chi.Mux {
	r := chi.NewMux()

	r.Get("/questions", services.GetQuestionsHandler)
	r.Post("/createQuestion", services.CreateQuestionHandler)
	r.Get("/questions/{id}", services.GetQuestionByIDHandler)
	r.Post("/createAnswer", service.CreateAnswerHandler)
	r.Get("/getAll", services.GetquestionANDanswer)
	r.Post("/createCategory", servicescategory.CreateCategoryHandler)

	return r
}