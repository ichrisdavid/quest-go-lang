package questions
//APLICATIONS
import (
	"github.com/kanwulf01/go-hexagonal-arq-test/helpers"
	"database/sql"
	"encoding/json"
	// "github.com/go-chi/chi"
	// "log"
	"fmt"
	"net/http"
	// "strconv"
	"github.com/kanwulf01/go-hexagonal-arq-test/questions/gateway"
	models "github.com/kanwulf01/go-hexagonal-arq-test/questions/models"
	"strings"
	//"github.com/gorilla/mux"
	"github.com/go-chi/chi"
	"strconv"
)

type QuestionHTTPService struct {
	gtw questions.QuestionGateway
}

type DataPregunta struct {
	Success bool          `json:"success"`
	DataPregunta    []models.Question `json:"pregunta"`
	Errors  []string      `json:"errors"`
}

type Pregunta struct {
	Success bool          `json:"success"`
	DataPregunta    models.Question `json:"pregunta"`
	Errors  []string      `json:"errors"`
}

func NewQuestionHTTPService(db *sql.DB) *QuestionHTTPService {
	return &QuestionHTTPService{
		questions.NewQuestionGateway(db),
	}
	
}

//controler que devuelve pacientes por peticion GET
func (s *QuestionHTTPService) GetQuestionsHandler(w http.ResponseWriter, r *http.Request) {
	p := s.gtw.GetQuestions()
	fmt.Println(p)
	if p == nil || len(p) == 0 {
		p = []*models.Question{}
	}
	json, _ := json.Marshal(p)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
}

// Get Question por ID
func (s *QuestionHTTPService) GetQuestionByIDHandler(w http.ResponseWriter, r *http.Request) {

	//vars := mux.Vars(r)
	//id := vars["id"]
	id := chi.URLParam(r, "id")
	var Pregunta = Pregunta{Errors: make([]string, 0)}
	iid,_ := strconv.ParseInt(id,10,64)
	fmt.Println(id)
	if !helpers.IsValidFieldNumeric(iid) {
		Pregunta.Success = false
		Pregunta.Errors = append(Pregunta.Errors, "invalid id")

		json, _ := json.Marshal(Pregunta)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	}

	q := s.gtw.GetQuestionByID(iid)
	fmt.Println(q)
	if q == nil {
		q = &models.Question{}
	}
	Pregunta.Success = true
	Pregunta.DataPregunta = *q

	json, _ := json.Marshal(Pregunta)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
	

}

//controller create Questions
func (s *QuestionHTTPService) CreateQuestionHandler(w http.ResponseWriter, r *http.Request) {
	bodyQuestion, success := helpers.DecodeBodyPregunta(r)
	if success != true {
		http.Error(w, "could not decode body", http.StatusBadRequest)
		return
	}
	fmt.Println(bodyQuestion)
	//validar los campos
	var DataPregunta = DataPregunta{Errors: make([]string, 0)}
	bodyQuestion.NombreP = strings.TrimSpace(bodyQuestion.NombreP)
	bodyQuestion.DescripcionP = strings.TrimSpace(bodyQuestion.DescripcionP)
	bodyQuestion.Respuesta = strings.TrimSpace(bodyQuestion.Respuesta)
	fmt.Println(bodyQuestion)
	if !helpers.IsValidField(bodyQuestion.DescripcionP) || !helpers.IsValidField(bodyQuestion.NombreP) || !helpers.IsValidField(bodyQuestion.Respuesta){
		
		DataPregunta.Success = false
		DataPregunta.Errors = append(DataPregunta.Errors, "invalid description")

		json, _ := json.Marshal(DataPregunta)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	}

	//var quest models.Question

	q,err := s.gtw.CreateQuestion(&bodyQuestion)
	if err != nil{
		DataPregunta.Errors = append(DataPregunta.Errors, "Cannot create Question in DB")
	}

	fmt.Println(q)

	DataPregunta.Success = true
	DataPregunta.DataPregunta = append(DataPregunta.DataPregunta,*q)

	json, _ := json.Marshal(DataPregunta)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
	return
}

func (s *QuestionHTTPService) GetquestionANDanswer(w http.ResponseWriter, r *http.Request) {
	q := s.gtw.Getquestionanswer()
	//fmt.Println(q)
	if q == nil {
		
	}
	json, _ := json.Marshal(q)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
	

}