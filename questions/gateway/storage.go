package questions

import (
	"fmt"
	"database/sql"
	"github.com/kanwulf01/go-hexagonal-arq-test/questions/models"
	"github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
	"log"
	//"time"
	
)

//1. crear interface

//2. crear objeto que trae la conexion a base de datos

//3. crear funciones de casos de uso para questions

type QuestionStorage interface {
	createQuestionDB(q *questions.Question) (*questions.Question, error)
	getQuestionsBD() []*questions.Question
	getQuestionByIDBD(id int64) (*questions.Question)
	GetquestionANDanswerBD() (*questions.QuestionxAnswers)
}

type QuestionService struct {
	db *sql.DB
}

func NewQuestionStorageGateway(db *sql.DB) QuestionStorage {
	return &QuestionService{db: db}
}

func (s *QuestionService) createQuestionDB(q *questions.Question) (*questions.Question, error) {
	res, err := s.db.Exec("insert into pregunta(nombrep, descripcionp, respuesta) values($1,$2,$3)",q.NombreP, q.DescripcionP,q.Respuesta)

	if err != nil {
		log.Printf("cannot save the question, %s", err.Error())
		return nil, err
	}

	id, err := res.LastInsertId()

	return &questions.Question{
		ID:        		id,
		NombreP:   		q.NombreP,
		DescripcionP:  	q.DescripcionP,
		Respuesta:		q.Respuesta,
	}, nil
}

func (s *QuestionService) getQuestionsBD() []*questions.Question {
	rows, err := s.db.Query("select id, nombrep, descripcionp, respuesta from pregunta")

	if err != nil {
		log.Printf("cannot execute select query: %s", err.Error())
		return nil
	}
	defer rows.Close()
	
	var qArray []*questions.Question
	for rows.Next() {
		q := questions.Question{}
		err := rows.Scan(&q.ID,&q.NombreP,&q.DescripcionP,&q.Respuesta)
		//fmt.Println(q.ID,q.NombreP,q.DescripcionP,q.Respuesta)
		if err != nil {
			log.Println("cannot read current row")
			return nil
		}
		qArray = append(qArray, &q)
	}
	fmt.Println(len(qArray))

	return qArray
}

//Retorna pregunta por ID
func (s *QuestionService) getQuestionByIDBD(id int64) (*questions.Question) {

	rows, err := s.db.Query("SELECT * FROM pregunta WHERE id=$1",id)
	if err != nil {
		log.Printf("cannot execute select query: %s", err.Error())
		return nil
	}

	defer rows.Close()

	var pregunta questions.Question

	for rows.Next() {

		err := rows.Scan(&pregunta.ID,&pregunta.NombreP,&pregunta.DescripcionP,&pregunta.Respuesta)
		if err != nil {
			log.Println("cannot read current row")
			return nil
		}
	}


	return &pregunta
}

//Retorna pregunta y sus respuestas por ID

func (s *QuestionService) GetquestionANDanswerBD() (*questions.QuestionxAnswers) {

	query,err := s.db.Query("select * from pregunta p inner join respuesta r on p.id = r.id_pregunta order by p.id asc")
	if err != nil {
		log.Println("Error in the query")
	}

	defer query.Close()

	encapsulaQA := questions.QuestionxAnswers{}//declaro un objeto de tipo que encapsula questionxrespuestas
	//objectQA := questions.ArrayQuestionxAnswers{}//declaro objeto que encapsula {questions:"algo",respuesta:"algo"}
	//var ArrayobjectQA []questions.ArrayQuestionxAnswers//instancio variable de tipo obecjto
	//var newArrayobjectQA []questions.ArrayQuestionxAnswers
	q := questions.Question{}
	var arrayrespuestas []answers.Answer//arreglo de respuestas
	//encapsula objeto pregunta y arreglo respuestas
	//llamare este objeto arrayQR
	//arrayQR := questions.QuestionxAnswersArray{}
	//objeto que encapsula el objeto de arriba lo llamare arrayObjectQR
	//arrayObjectQR := questions.QAarrayObject{}


	for query.Next() {
		//mapear los datos que estan en el query
		r := answers.Answer{}
		err := query.Scan(&q.ID,&q.NombreP, &q.DescripcionP, &q.Respuesta, &q.Id_categoria, &q.Puntos , &q.Time,&r.ID,&r.Respuesta,&r.Id_pregunta)
		if err != nil {
			log.Println("cannot read current row")
			
		}

		arrayrespuestas = append(arrayrespuestas,r)//lleno el arreglo de objetos respuestas
		// encapsulaQA.Question1 = q//llena el objeto pregunta
		// encapsulaQA.Respuestas = r//llena el objeto respuesta

		// objectQA.Data = encapsulaQA//llena el objeto que los encapsula
		// ArrayobjectQA = append(ArrayobjectQA,objectQA)//llena el arreglo de objetos encapsulador

	}
	fmt.Println(q)

	encapsulaQA.Question1 = q
	encapsulaQA.Respuestas = arrayrespuestas


	
	return &encapsulaQA


}
