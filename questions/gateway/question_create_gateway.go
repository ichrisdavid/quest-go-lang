package questions
//INFRAESTRUCTURA
import (
	"database/sql"
	"github.com/kanwulf01/go-hexagonal-arq-test/questions/models"
)

type QuestionGateway interface {
	CreateQuestion(q *questions.Question) (*questions.Question, error)
	GetQuestions()([]*questions.Question)
	GetQuestionByID(id int64) (*questions.Question)
	Getquestionanswer() (*questions.QuestionxAnswers)

}
//objeto que implementa la interface que tiene los metodos
/*
createQuestionDB(q *questions.Question) (*questions.Question, error)
getQuestionsBD() []*questions.Question
*/
type CreateQuestionInDB struct {
	QuestionStorage
}

func (c *CreateQuestionInDB) GetQuestions() []*questions.Question {
	return c.getQuestionsBD()
}

func NewQuestionGateway(db *sql.DB) QuestionGateway {
	return &CreateQuestionInDB{NewQuestionStorageGateway(db)}
}

func (c *CreateQuestionInDB) CreateQuestion(q *questions.Question) (*questions.Question, error) {
	return c.createQuestionDB(q)
}

func (c *CreateQuestionInDB) GetQuestionByID(id int64) *questions.Question {
	return c.getQuestionByIDBD(id)
}

func (c *CreateQuestionInDB) Getquestionanswer() (*questions.QuestionxAnswers) {
	return c.GetquestionANDanswerBD()
}