package questions

import (
	models "github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
	"time"
)

//Objeto de tipo Pregunta
type Question struct {
	ID          int64    `json:"id"`
	Pregunta_uid	string 	 `json:"uid"`
	NombreP string `json:"nombrep"`
	DescripcionP string `json:"descripcionp"`
	Respuesta string `json:"respuesta"`
	Id_categoria string 	`json:"id_categoria"`
	Puntos 		int64    `json:"puntos"`
	Time 		time.Time   `json:"time"`

}

//Objeto tipo Join Pregunta x Respuesta
type QuestionxAnswers struct {
	Question1 Question `json:"pregunta"`
	Respuestas []models.Answer `json:"respuestas"`
}


//Objeto que atrapa en arreglo el objeto QuestionxAnswers
type ArrayQuestionxAnswers struct {
	Data QuestionxAnswers `json:"data"`
}

////
/*
type QuestionxAnswersArray struct {
	Question1 Question `json:"pregunta"`
	Respuestas []models.Answer `json:"respuestas"`
}

type QAarrayObject struct {
	Data QuestionxAnswersArray `json:"data"`
}
*/