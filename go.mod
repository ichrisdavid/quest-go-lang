module github.com/kanwulf01/go-hexagonal-arq-test

go 1.14

require (
	github.com/bmizerany/pq v0.0.0-20131128184720-da2b95e392c1
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/satori/go.uuid v1.2.0
	github.com/tomiok/patients-API v0.0.0-20200524022646-b3928a99a8c8
)
