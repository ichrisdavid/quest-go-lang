package helpers

import (
	"encoding/json"
	models "github.com/kanwulf01/go-hexagonal-arq-test/questions/models"
	"net/http"
	answers "github.com/kanwulf01/go-hexagonal-arq-test/answers/models"
	category "github.com/kanwulf01/go-hexagonal-arq-test/category/models"
	"strings"
)

//Decodificacion del modelo Pregunta
func DecodeBodyPregunta(req *http.Request) (models.Question, bool) {
	var pregunta models.Question
	err := json.NewDecoder(req.Body).Decode(&pregunta)
	if err != nil {
		return models.Question{},false
	}

	return pregunta, true
}

//Decodificacion del modelo Respuesta

func DecodeBodyRespuesta(req *http.Request) (answers.Answer, bool) {
	var respuesta answers.Answer
	err := json.NewDecoder(req.Body).Decode(&respuesta)
	if err != nil {
		return answers.Answer{},false
	}

	return respuesta, true
}

func DecodeBodyCategory(req *http.Request) (category.Categoria, bool) {
	var categoria category.Categoria
	err := json.NewDecoder(req.Body).Decode(&categoria)
	if err != nil {
		return category.Categoria{},false
	}

	return categoria, true
}


func IsValidFieldNumeric(field int64) bool {
	if field == 0 {
		return false
	}

	return true
}

//Valida los campos de tipo string si son vacios
func IsValidField(field string) bool {
	desc := strings.TrimSpace(field)
	if len(desc) == 0 {
		return false
	}

	return true
}